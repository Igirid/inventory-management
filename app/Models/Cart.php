<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'uuid',
        'user_uuid',
        'subtotal',
    ];

    public function cartItems()
    {
        return $this->hasMany('App\Models\CartItem', 'cart_uuid', 'uuid');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_uuid', 'uuid');
    }
}
