<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'uuid',
        'user_uuid',
        'inventory_uuid',
        'cart_uuid',
        'total_price',
        'price',
        'quantity'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        // 'email_verified_at' => 'datetime',
    ];

    protected $appends = [];


    public function inventory()
    {
        return $this->belongsTo('App\Models\Inventory', 'inventory_uuid', 'uuid');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_uuid', 'uuid');
    }

    public function cart()
    {
        return $this->belongsTo('App\Models\Cart', 'cart_uuid', 'uuid');
    }
}
