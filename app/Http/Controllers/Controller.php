<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use Illuminate\Support\Str;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function generateUUid(){
        return Str::uuid()->toString();
    }

    protected function createCart($user){
        return Cart::create([
            'user_uuid' => $user->uuid,
            'uuid' => $this->generateUUid(),
            'subtotal' => 0,
        ]);
    }
}
