<?php

namespace App\Http\Controllers\Api\Users;

use App\Models\Inventory;
use App\Http\Controllers\Controller;


class InventoryController extends Controller
{
    public function listing()
    {
        $inventory = Inventory::where('quantity', '>', 0)->paginate(15);

        return $inventory;
    }

    public function view(Inventory $inventory)
    {
        if ($inventory) {
            return response()->json(['message' => 'Inventory fetched', 'data' => $inventory]);
        }
        return redirect("/api/admins/inventories")->withErrors(['message' =>  'Not found']);
    }
}
