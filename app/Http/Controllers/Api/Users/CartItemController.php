<?php

namespace App\Http\Controllers\Api\Users;

use App\Models\Cart;
use App\Models\User;
use App\Models\CartItem;
use App\Models\Inventory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\CartItemCreateRequest;

class CartItemController extends Controller
{
    public function create(CartItemCreateRequest $request)
    {
        // only authenticated users can access this 

        $user = $request->user('user');
        $cart = $user->load('cart')->cart;
        $inventory = Inventory::where('uuid', $request->inventory_uuid)->first();

        if ($user && !$cart) {
            $cart = $this->takeCart($user);
        }

        $data = $request->validated();
        $data['uuid'] = $this->generateUUid();
        $data['cart_uuid'] = $cart->uuid;
        $data['price'] = $inventory->price;
        $data['total_price'] = intval($data['quantity']) * intval($data['price']);


        $cartItem = $cart->cartItems()->where('inventory_uuid', $inventory->uuid)->first();

        if (!blank($cartItem)) {
            $cartItem->quantity += intval($data['quantity']);
            $cartItem->total_price += $data['total_price'];
            $cartItem->save();
        } else {
            $cartItem = CartItem::create($data);
        }

        $this->updateCart($cart, $data['total_price']);

        return response()->json(['message' => 'Cart item added', 'data' => $cartItem->refresh()]);
    }

    public function view(CartItem $cartItem)
    {
        if ($cartItem) {
            return response()->json(['message' => 'Cart item fetched', 'data' => $cartItem]);
        }
        return redirect('/api/users/cart/')->withErrors(['message' => 'Not found']);
    }

    public function delete(CartItem $cartItem)
    {
        if ($cartItem && $this->removeCartItemFromCart($cartItem)) {
            return response()->json(['message' => 'Cart item removed', 'data' => $cartItem]);
        }
        return redirect('/api/users/cart/')->withErrors(['message' => 'An error occured']);
    }

    protected function takeCart(User $user)
    {
        return Cart::create([
            'user_uuid' => $user->uuid,
            'uuid' => $this->generateUUid(),
            'subtotal' => 0,
        ]);
    }

    protected function updateCart($cart, $total_price)
    {
        $cart->subtotal += $total_price;
        $cart->save();
    }

    protected function removeCartItemFromCart(CartItem $cartItem)
    {
        try {
            //code...
            DB::beginTransaction();
            $cart = $cartItem->load('cart')->cart;
            $cart->subtotal -= $cartItem->total_price;
            $cart->save();
            $cartItem->delete();
            DB::commit();
            return true;
        } catch (\Throwable $th) {
            DB::rollBack();
            dd($cartItem);
            throw $th;
            return false;
        }
    }
}
