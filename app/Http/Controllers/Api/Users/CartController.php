<?php

namespace App\Http\Controllers\Api\Users;

use App\Models\Cart;
use App\Models\Inventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{

    public function create(Request $request)
    {
        $user = $request->user('user');

        $cart = $this->createCart($user);

        return response()->json(['message' => 'Cart created', 'data' => $cart]);
    }

    public function view(Request $request)
    {
        $user = $request->user('user');
        $cart = $user->cart;
        if (!$cart) {
            $cart = $this->createCart($user);
        }
        return response()->json(['message' => 'Cart fetched', 'data' => $cart->load('cartItems')]);
    }

    public function checkout(Request $request)
    {
        $user = $request->user('user');

        if ($user && $user->load('cart')->cart) {
            $user->cart->cartItems->map(function ($cartItem) {
                $inventory = $cartItem->inventory;
                $inventory->quantity -= $cartItem->quantity;
                $inventory->save();
                $cartItem->delete();
            });
            $cart = $user->cart;
            $cart->subtotal = 0;
            $cart->save();
            return response()->json(['message' => 'Checkout complete!', 'data' => $cart]);
        }
        return redirect("/api/users/{$user->uuid}/cart")->withErrors(['message' => 'An error occured']);
    }

    public function delete(Request $request)
    {
        $user = $request->user('user');
        $cart = $user->cart;
        if ($cart) {
            $cart->cartItems->map(fn($cartItem)=> $cartItem->delete());
            $cart->delete();
            return response()->json(['message' => 'Cart deleted', 'data' => $user->cart]);
        }
        abort(404);
    }
}
