<?php

namespace App\Http\Controllers\Api\Admins;

use App\Models\Inventory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\InventoryCreateRequest;
use App\Http\Requests\InventoryUpdateRequest;

class InventoryController extends Controller
{
    public function listing()
    {
        $inventory = Inventory::paginate(15);

        return $inventory;
    }

    public function create(InventoryCreateRequest $request)
    {

        $data = $request->validated();
        $data['uuid'] = $this->generateUUid();
        $inventory = Inventory::create($data);

        return response()->json(['message' => 'Inventory Created', 'data' => $inventory]);
    }

    public function update(InventoryUpdateRequest $request, Inventory $inventory)
    {
        $data = $request->validated();
        if ($inventory && $inventory->update($data)) {
            return response()->json(['message' => 'Inventory updated', 'data' => $inventory]);
        }
        return redirect("/api/admins/inventories")->withErrors(['message' => 'Not found']);
    }

    public function view(Inventory $inventory)
    {
        if ($inventory) {
            return response()->json(['message' => 'Inventory fetched', 'data' => $inventory]);
        }
        return redirect("/api/admins/inventories")->withErrors(['message' =>  'Not found']);
    }

    public function delete(Inventory $inventory)
    {
        if ($inventory && $inventory->delete()) {

            return response()->json(['message' => 'Inventory deleted', 'data' => $inventory]);
        }
        return redirect('/api/admins/inventories')->withErrors(['message' => 'Not found']);
    }
}
