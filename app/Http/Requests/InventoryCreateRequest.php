<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class InventoryCreateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $admin = $this->user('admin');

        return [
            'name' => [
                'required', 'string', 'max:255',
            ],
            'price' => [
                'required', 'numeric', 'min:1',
            ],
            'quantity' => [
                'required', 'integer',
            ],
        ];
    }
}
