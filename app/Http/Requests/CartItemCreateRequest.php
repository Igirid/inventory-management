<?php

namespace App\Http\Requests;

use App\Models\Inventory;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CartItemCreateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->inventory = Inventory::where('uuid', $this->inventory_uuid)->first();

        return $this->inventory && $this->inventory->quantity > 0;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'inventory_uuid' => [
                'required', 'uuid',
            ],
            'user_uuid' => [
                'required', 'uuid',
            ],
            // 'cart_uuid' => [
            //     'required', 'uuid',
            // ],
            'quantity' => [
                'required', 'integer', 'min:1', "lte:{$this->inventory->quantity}"
            ],
            // 'price' => [
            //     'required', 'integer', 'min:1'
            // ],
            // 'total_price' => [
            //     'required', 'integer', 'min:1'
            // ],
            // 'admin_uuid' => ['required', 'uuid', Rule::exists('shows', 'uuid')],
        ];
    }
}
