<?php

namespace App\Http\Requests;

use App\Models\Inventory;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class InventoryUpdateRequest extends FormRequest
{
    protected $inventory = '';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => [
                'required', 'string', 'max:255',
            ],
            'price' => [
                'required', 'numeric', 'min:1',
            ],
            'quantity' => [
                'required', 'integer',
            ],
            // 'uuid' => ['required', 'uuid', Rule::exists('shows', 'uuid')],
        ];
    }
}
