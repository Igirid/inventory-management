<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CartItemCreateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subtotal' => [
                'required', 'numeric',
            ],
            'user_uuid' => [
                'required', 'uuid',
            ],
            // 'quantity' => [
            //     'required', 'integer', 'min:1'
            // ],
            // 'admin_uuid' => ['required', 'uuid', Rule::exists('shows', 'uuid')],
        ];
    }
}
