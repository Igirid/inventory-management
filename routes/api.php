<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/', fn () => 'working');
Route::get('/documentation', fn () => 'docx');

Route::namespace('App\Http\Controllers\Api')->group(function () {

    Route::prefix('admins')->name('admins.')->namespace('Admins')->group(function () {
        //Authentiction
        Route::namespace('Auth')->group(function () {
            Route::post('login', 'AuthController@login');
        });

        Route::middleware(['auth:admin'])->group(function () {

            Route::namespace('Auth')->group(function () {
                Route::get('logout', 'AuthController@logout');
            });

            //Inventories
            Route::prefix('inventories')->name('inventories.')->group(function () {
                Route::get('/', 'InventoryController@listing');
                Route::get('{inventory:uuid}/view', 'InventoryController@view');
                Route::post('create', 'InventoryController@create');
                Route::put('{inventory:uuid}/update', 'InventoryController@update');
                Route::delete('{inventory:uuid}/delete', 'InventoryController@delete');
            });
        });
    });

    Route::prefix('users')->name('users.')->namespace('Users')->group(function () {

        //Authentiction
        Route::namespace('Auth')->group(function () {
            Route::post('login', 'AuthController@login');
            Route::post('register', 'AuthController@register');
        });

        //cart

        Route::middleware(['auth:user'])->group(function () {

            Route::namespace('Auth')->group(function () {
                Route::get('logout', 'AuthController@logout');
            });

            Route::prefix('inventories')->name('inventories.')->group(function () {
                Route::get('/', 'InventoryController@listing');
                Route::get('{inventory:uuid}/view', 'InventoryController@view');
            });

            Route::prefix('cart')->name('carts.')->group(function () {
                Route::get('/', 'CartController@view');
                Route::post('checkout', 'CartController@checkout');
                Route::delete('delete', 'CartController@delete');

                //cart items
                Route::prefix('cart-items')->name('cart_items.')->group(function () {
                    Route::get('{cartItem:uuid}/view', 'CartItemController@view');
                    Route::post('add', 'CartItemController@create');
                    Route::delete('{cartItem:uuid}/delete', 'CartItemController@delete');
                });
            });
        });
    });
});
