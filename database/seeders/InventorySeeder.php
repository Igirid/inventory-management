<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Inventory;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Admin::where('email', 'careers@ncktech.com')->first();

        $names = [
            'Suede VTG',
            'Bubblegum Mule',
            'Courtney Flatform Slide',
            'Eugenia Flat Croco Miel',
            'Regina Clog Slide Sandals',
            'Gladiator with Ankle-Tie Sandal',
            'Prism Faux Fur Slide',
            'Paily Heels',
            'Classic Suede Sneakers',
            'Florida Soft Footbed',
            'Yin Meets Yang',
            'Palmella Pump in Sky',
            'Womens Techloom Breeze White',
            'Coryna Peep-Toe Patent Leather Platform Sandal',
            'Unisex Chuck Taylor All Star High Top',
            'The Felize Mule',
            'Alicia Flatforms',
            'Renew Run Womens Running Shoes',
            'The Point',
            'Womens 2750 Cotu Classic Sneaker',
            'Fluff Yeah Slide Slipper, cream',
            'Sable Campbell',
            'Original Tall Rain Boot Black Gloss',
            'Fifi Sandal',
            'Womens Wool Runners',
            'Jamila Strappy Heel',
            'V-10 Lace Up Sneakers',
            'Naima Crystal-Embellished Suede Sandals',
        ];


        for ($i = 0; $i < 26; $i++) {
            $inventory = Inventory::create([
                'name' => $names[$i],
                'price' => rand(50, 70),
                'quantity' => 5,
                'uuid' =>  Str::uuid()->toString(),
                'created_at' => now()
            ]);
        }
    }
}
