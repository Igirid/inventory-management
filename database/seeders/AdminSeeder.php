<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'first_name' => 'Favour',
            'last_name' => 'Nck',
            'password' => Hash::make('password'),
            'email' => 'careers@ncktech.com',
            'uuid' => Str::uuid()->toString(),
            'created_at' => now(),
        ]);
    }
}
