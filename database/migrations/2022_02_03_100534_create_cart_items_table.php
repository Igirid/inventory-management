<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_items', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->foreignUuid('user_uuid')->nullable()->on('users')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreignUuid('inventory_uuid')->nullable()->on('inventories')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreignUuid('cart_uuid')->nullable()->on('carts')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->float('total_price');
            $table->float('price');
            $table->float('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_items');
    }
}
